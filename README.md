# TorSwitch
WebExtension to browse through Tor using a SOCKS5 proxy.

# License
This project is licensed under the terms of the GNU General Public License v3.0.

# Credits
* Toolbar icon by [BlackVariant](https://www.deviantart.com/blackvariant)
* Information icon by [Mozilla Firefox](https://design.firefox.com/icons/viewer/)
